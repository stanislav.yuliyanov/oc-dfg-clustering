# cluster products and showing all cli options
python ./code/cli.py ./data/ocel.jsonocel products all --attr_weights '{\"costs\": 2, \"producer\":10}' --clustering_mode kmeans --cluster_count auto --max_cluster_count 10 --ocel_file_type json --graph_file_type svg --graph_edge_threshold 500 --graph_activity_threshold 0 --target_dir data/ocels_clustered

# fast products clustering (useful for debugging)
python ./code/cli.py ./data/ocel.jsonocel products all --max_cluster_count 10 --target_dir data/ocels_clustered

# all items clustering (less than a minute), cluster count fix set to 28
python ./code/cli.py ./data/ocel.jsonocel items all --cluster_count 28 --graph_edge_threshold 0 --target_dir data/ocels_clustered --graph_file_type svg

# getting help (documentation of the parameters)
python ./code/cli.py --help
