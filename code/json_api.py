import argparse
from math import floor
import math
import shutil
import tempfile
from flask import Flask, Response, request, jsonify, send_file
import os
import ocel_clustering.main as ocel_clustering
from werkzeug.utils import secure_filename
from os.path import exists
import pathlib
import pm4py
from pm4py.objects.ocel.obj import OCEL

DEFAULT_UPLOAD_FOLDER   = 'uploads'
DEFAULT_PORT            = 5000
DEFAULT_DEBUG_MODE      = True
DEFAULT_EMPTY_GRAPH = {
    'svg': os.path.dirname(os.path.abspath(__file__)) + '/defaults/empty_ocdfg.svg',
    'png': os.path.dirname(os.path.abspath(__file__)) + '/defaults/empty_ocdfg.png'
}

ALLOWED_EXTENSIONS      = {'jsonocel', 'xmlocel'}
RESULT_FOLDER_SUFFIX    = '_results'
DOMAIN_WHITELIST        = ['localhost', '127.0.0.1', 'localhost:3000']

PARAM_FILE_NAME         = 'params.json'
CLUSTERING_PLOT_FILE_NAME = 'clustering_plot.svg'
CLUSTERING_SCATTER_FILE_NAME = 'clustering_scatter.svg'

def validate_directory(dir: str) -> str:
    res = dir
    while(len(res) > 0 and res[0] == '/'):
        res = res[1:]
    while(len(res) > 0 and res[-1] == '/'):
        res = res[:len(res)-1]
    if not exists(res):
        os.makedirs(res)
    return str(os.getcwd() + '/' + res)

CLI=argparse.ArgumentParser()
CLI.add_argument(
    "--upload_folder",
    type=validate_directory,
    default=DEFAULT_UPLOAD_FOLDER,
    help='The folder where the API-handler stores files.',
    required=False
)
CLI.add_argument(
    "--port",
    type=int,
    default=DEFAULT_PORT,
    help='The Port which the API uses.',
    required=False
)
CLI.add_argument(
    "--debug",
    action="store_true",
    default=False,
    help='Defines if flask is used in debug mode or not.',
    required=False
)
args = CLI.parse_args()

print("debug:         " + str(args.debug))
print("port:          " + str(args.port))
print("upload_folder: " + str(args.upload_folder))

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = str(args.upload_folder)

# ensure existence of upload folder
if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def create_error_json(id: str, description: str) -> Response:
    return jsonify({
        'errors': [{
            'id': id,
            'description': description
        }]
    })

from urllib.parse import urlparse
def get_domain_from_url(url: str) -> str:
    parsed = urlparse(url)
    netloc = parsed.netloc
    domain = '.'.join(netloc.split('.')[-2:]) # last two elements 
    return domain

import json as json
def get_hash_of_params(params: dict) -> int:
    json_string = json.dumps(params)
    return hash(json_string)

# returns ordered list of result-files without directory or anything.
# valid results cant be empty. if empty list is returned, there exeists no results.
def get_result_files_for(original_ocel_file_name: str, clustering_id: str) -> list[str]:
    s_filename = secure_filename(original_ocel_file_name)
    if not exists(app.config['UPLOAD_FOLDER'] + '/' + s_filename):
        raise Exception('file does not exists')
    original_ext = pathlib.Path(s_filename).suffix[1:]
    folder = app.config['UPLOAD_FOLDER'] + '/' + s_filename + RESULT_FOLDER_SUFFIX + '/' + clustering_id
    res = []
    if exists(folder):
        for file in sorted(os.listdir(folder)):
            ext = pathlib.Path(file).suffix[1:]
            if ext == original_ext: res.append(file)
    return res

def get_suffix(filename: str) -> str:
    return pathlib.Path(filename).suffix[1:]

############
## ROUTES ##
############
@app.route("/")
def hello_world():
    return 'Hello World'

############
## UPLOAD ##
############

@app.route('/ocel-upload', methods=['GET'])
def ocel_upload_get() -> Response:
    """
    Returns a list of all files already uploaded to the upload folder.
    
    Returns
    -------
    Response
        A flask response which consists of a json with a attribute data which consists of a list. Every Element in the list has two attributes: id and type.
    """
    res = []
    for file in os.listdir(app.config['UPLOAD_FOLDER']):
        ext = pathlib.Path(file).suffix[1:]
        if ext in ALLOWED_EXTENSIONS:
            res.append({
                'id': file,
                'type': ext
            })
    return jsonify({
        'data': res
    })

@app.route('/ocel-upload', methods=['POST'])
def ocel_upload_post():
    # check if the post request has the file part
    if 'file' not in request.files:
        return create_error_json('no-file-attribute', 'The request does not contain a file attribute.'), 404
    file = request.files['file']
    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    if file.filename == '':
        return create_error_json('empty-filename', 'The filename of the given file is empty'), 404
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        full_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        if(exists(full_filename)):
            return create_error_json('file-already-exists', 'There exists already file with the given name. Use the file or delete it via the DELETE method.'), 404
        file.save(full_filename)
        return jsonify({
            'data': {
                'id': filename,
                'type': pathlib.Path(filename).suffix[1:]
            }
        })
    else: return create_error_json('file-type-error', 'The file type is not supported. Use one of: ' + ', '.join(ALLOWED_EXTENSIONS)), 404

@app.route('/ocel-upload/<filename>', methods=['DELETE'])
def ocel_upload_delete(filename):
    """Deletes the file given in the filename-argument from the server. """
    filename = secure_filename(filename)
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    if exists(full_filename):
        os.remove(full_filename)
        return jsonify({
            'data': [
                {
                    'id': filename,
                    'type': pathlib.Path(filename).suffix[1:]
                }
            ]
        })
    else: return create_error_json('file-not-found', 'The given file does not exist.'), 404

# todo: delete all files that are inside the folder before
# todo: create id from parameters and filename (and/or filecontent?) and return this id as result id and store to 
# <filename>_results/<id>/<result_x>.<type>ocel 
@app.route("/ocel-cluster/<filename>", methods=['POST'])
def ocel_cluster_post(filename):
    s_filename = secure_filename(filename)
    
    ocel = get_ocel_from_filename(s_filename)

    JSON_sent = request.get_json().get('data', {})

    hash_val = str(floor(math.floor(get_hash_of_params(JSON_sent))))
    json_config_string = json.dumps(JSON_sent, indent=4)

    object_type = JSON_sent.get('object-type', None)
    if object_type == None:
        raise Exception('"object-type" attribute missing')
    
    mode = JSON_sent.get('mode', None)
    if mode == None:
        raise Exception('"mode" attribute missing.')
    # getting attribute weights and setting accordingly
    attr_weights = JSON_sent.get('attr-weights', {})
    attr_def = ocel_clustering.ocel_get_attr_def(ocel)
    def set_weights_function(attr_def: dict) -> dict:
        res = attr_def
        if res['name'] in attr_weights.keys():
            res['weight'] = attr_weights[res['name']]
        return res
    attr_def = list(map(set_weights_function, attr_def))

    # setting control_flow_weight
    control_flow_weight = attr_weights.get(ocel_clustering.OCEL_COL_NAME_CONTROL_FLOW, 1.0)

    clustering_algo = JSON_sent.get('clustering-algorithm', 'kmeans')
    clustering_evaluation_mode = JSON_sent.get('clustering-evaluation', 'silhouette')

    # executing core clustering
    res, distance_matrix, cluster_evaluation = ocel_clustering.ocel_cluster_by_objects(
        ocel=ocel, 
        object_type             = object_type, 
        event_assignment_mode   = mode,
        attr_def                = attr_def, 
        clustering_algorithm    = clustering_algo,
        evaluate                = clustering_evaluation_mode,
        max_cluster_count       = JSON_sent.get('max-cluster-count', -1),
        cluster_count           = JSON_sent.get('cluster-count', -1),
        control_flow_active     = control_flow_weight != 0.0,
        control_flow_weight     = control_flow_weight
    )

    # storing result files
    target_dir = app.config['UPLOAD_FOLDER'] + '/' + s_filename + RESULT_FOLDER_SUFFIX + '/' + str(hash_val) # storing in a folder named according to file
    if os.path.exists(target_dir):
        shutil.rmtree(target_dir) # deleting all from previous calculations
    os.makedirs(target_dir)

    # storing configuration
    config_file = open(target_dir + '/' + PARAM_FILE_NAME, 'w')
    config_file.write(json_config_string)
    config_file.close()

    # storing ocel files
    appendix_len = len(str(len(res)))
    for ii in range(0, len(res)):
        res_filename = target_dir + '/' + str(ii+1).rjust(appendix_len, '0') + '.' + pathlib.Path(s_filename).suffix[1:]
        pm4py.write_ocel(res[ii], res_filename)
    
    # storing anaylis files
    res_filename = target_dir + '/' + CLUSTERING_PLOT_FILE_NAME
    cluster_evaluation.plot(savefig={'fname': res_filename, 'format': 'svg'})

    res_filename = target_dir + '/' + CLUSTERING_SCATTER_FILE_NAME
    cluster_evaluation.scatter(distance_matrix, savefig={'fname': res_filename, 'format': 'svg'})

    # returning
    return jsonify({
        'data': {
            'id': hash_val,
            'type': 'ocel-clustering'
        }
    })

# CLUSTERING-ROUTES

def get_ocel_from_filename(filename: str) -> OCEL:
    s_filename = secure_filename(filename)
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], s_filename)
    if not exists(full_filename) or not pathlib.Path(s_filename).suffix[1:] in ALLOWED_EXTENSIONS:
        raise Exception('The file does not exists')
    
    # reading OCEL-File
    ocel = pm4py.read_ocel(full_filename)
    return ocel

@app.route("/ocel-cluster/<filename>/object-types", methods=['GET'])
def ocel_cluster_get_object_types(filename):
    ocel = get_ocel_from_filename(filename)
    res = []
    for elem in ocel.objects["ocel:type"].unique():
        res.append({
            'id': str(elem),
            'type': 'ocel-type'
        })
    return jsonify({
        'data': res
    })

@app.route("/ocel-cluster/<filename>/object-types/<objecttype>/attributes", methods=['GET'])
def ocel_cluster_get_attributes(filename, objecttype):
    ocel = get_ocel_from_filename(filename)
    relevant_data = ocel.objects[ocel.objects[ocel_clustering.OCEL_COL_NAME_OBJECT_TYPE] == objecttype]
    relevant_data = relevant_data.drop(ocel_clustering.OCEL_COL_NAME_OBJECT_TYPE, axis=1).set_index(ocel_clustering.OCEL_COL_NAME_OID).dropna(how='all', axis=1)
    columns = relevant_data.columns
    res = []
    for attr in ocel_clustering.ocel_get_attr_def(ocel):
        attr_name = attr.get('name', '')
        if attr_name in columns:
            res.append({
                'id': attr_name,
                'type': 'ocel-object-attribute'
            })
    return jsonify({
        'data': res
    })

@app.route("/ocel-cluster/<filename>/object-types/<objecttype>/instance-count", methods=['GET'])
def ocel_cluster_get_object_count(filename, objecttype):
    ocel = get_ocel_from_filename(filename)
    relevant_data = ocel.objects[ocel.objects[ocel_clustering.OCEL_COL_NAME_OBJECT_TYPE] == objecttype]

    return jsonify({
        'data': {
            'id': 1,
            'type': 'ocel-object-instance-count',
            'attributes': {
                'count': relevant_data.shape[0]
            }
        } 
    })

@app.route("/ocel-cluster/<filename>/clustering/<clustering_id>/clusters", methods=['GET'])
def ocel_cluster_get(filename: str, clustering_id: str):
    s_filename = secure_filename(filename)
    if not exists(app.config['UPLOAD_FOLDER'] + '/' + s_filename):
        return create_error_json('file-not-found', 'The file does not exists.'), 404

    suffix = get_suffix(s_filename)
    file_list = sorted(get_result_files_for(s_filename, clustering_id))
    res = []
    for ii in range(len(file_list)):
        res.append({
            'id': s_filename + '-cluster-' + str(ii),
            'type': 'clustered-' + suffix,
            'attributes': {
                'filetype': suffix
            }
        })

    if len(res) == 0:
        return create_error_json('results-not-found', 'There are no results for the given file. Execute POST method first.'), 404
    else:
        return jsonify({
            'data': res
        })

# returns config for a clustering_id
@app.route("/ocel-cluster/<filename>/clustering/<clustering_id>", methods=['GET'])
def ocel_cluster_get_params(filename, clustering_id):
    full_filename = app.config['UPLOAD_FOLDER'] + '/' + filename + RESULT_FOLDER_SUFFIX + '/' + clustering_id + '/' + PARAM_FILE_NAME
    if not exists(full_filename): return create_error_json('file-not-found', 'Not found.')
    text_file = open(full_filename, 'r')
    json_object = json.load(text_file)
    text_file.close()
    return jsonify({
        'data': json_object
    })

@app.route("/ocel-cluster/<filename>/clustering/<clustering_id>/evaluation/plot", methods=['GET'])
def ocel_cluster_get_plot(filename, clustering_id):
    full_filename = app.config['UPLOAD_FOLDER'] + '/' + filename + RESULT_FOLDER_SUFFIX + '/' + clustering_id + '/' + CLUSTERING_PLOT_FILE_NAME
    if not exists(full_filename): return create_error_json('file-not-found', 'Not found.')
    return send_file(full_filename, download_name=CLUSTERING_PLOT_FILE_NAME)

@app.route("/ocel-cluster/<filename>/clustering/<clustering_id>/evaluation/scatter", methods=['GET'])
def ocel_cluster_get_scatter(filename, clustering_id):
    full_filename = app.config['UPLOAD_FOLDER'] + '/' + filename + RESULT_FOLDER_SUFFIX + '/' + clustering_id + '/' + CLUSTERING_SCATTER_FILE_NAME
    if not exists(full_filename): return create_error_json('file-not-found', 'Not found.')
    return send_file(full_filename, download_name=CLUSTERING_SCATTER_FILE_NAME)

# missing: type of file has to be set accordingly! json/xml
@app.route("/ocel-cluster/<filename>/clustering/<clustering_id>/clusters/<number>", methods=['GET'])
def ocel_cluster_get_result_file(filename, clustering_id, number):
    number = int(number)
    res_files = get_result_files_for(filename, clustering_id)
    if number >= len(res_files) or number < 0:
        return create_error_json('index-out-of-bounds', 'There is no cluster with index ' + str(number) + '. Use a value in between 0 and ' + str(len(res_files)-1) + '.'), 404
    full_filename = app.config['UPLOAD_FOLDER'] + '/' + filename + RESULT_FOLDER_SUFFIX + '/' + clustering_id + '/' + res_files[number]
    text_file = open(full_filename, 'r')
    data = text_file.read()
    text_file.close()
    # MISSING: DEFINE MIMETYPE! json/application or xml/text 
    return data    

# implement different graph formats
@app.route("/ocel-cluster/<filename>/clustering/<clustering_id>/clusters/<number>/graph", methods=['GET'])
def ocel_cluster_get_visualization(filename, clustering_id, number):
    suffix = '.svg'
    number = int(number)
    s_filename = secure_filename(filename)
    res_files = get_result_files_for(s_filename, clustering_id)
    if number >= len(res_files) or number < 0:
        return create_error_json('index-out-of-bounds', 'There is no cluster with index ' + str(number) + '. Use a value in between 0 and ' + str(len(res_files)-1) + '.'), 404

    full_filename = app.config['UPLOAD_FOLDER'] + '/' + s_filename + RESULT_FOLDER_SUFFIX + '/' + clustering_id + '/' + res_files[number]

    ocel = pm4py.read_ocel(full_filename)
    if ocel.events.shape[0] == 0:
        # no events -> empty dfg and save_vis_ocdfg fails on that.
        tmp_file = tempfile.NamedTemporaryFile(suffix=suffix, delete=False)
        shutil.copyfile(DEFAULT_EMPTY_GRAPH[args.graph_file_type],tmp_file)
        tmp_file.delete = True
    else:
        ocdfg = pm4py.discover_ocdfg(ocel)

        # tmp_file = tempfile.TemporaryFile(suffix=suffix, delete=False)
        tmp_file = tempfile.NamedTemporaryFile(suffix=suffix, delete=False)

        pm4py.save_vis_ocdfg(ocdfg, tmp_file.name, act_threshold=int(request.args.get('activity_threshold', 0)), edge_threshold=int(request.args.get('edge_threshold', 0)))
        tmp_file.delete = True
    return send_file(tmp_file, download_name=s_filename + '-cluster-' + str(number) + suffix)

@app.route("/ocel-cluster/<filename>/graph", methods=['GET'])
def ocel_get_visualization(filename):
    suffix = '.svg'
    s_filename = secure_filename(filename)

    full_filename = app.config['UPLOAD_FOLDER'] + '/' + s_filename

    ocel = pm4py.read_ocel(full_filename)
    ocdfg = pm4py.discover_ocdfg(ocel)

    # tmp_file = tempfile.TemporaryFile(suffix=suffix, delete=False)
    tmp_file = tempfile.NamedTemporaryFile(suffix=suffix, delete=False)

    pm4py.save_vis_ocdfg(ocdfg, tmp_file.name, act_threshold=int(request.args.get('activity_threshold', 0)), edge_threshold=int(request.args.get('edge_threshold', 0)))
    tmp_file.delete = True
    return send_file(tmp_file, download_name=s_filename + '-graph-' + suffix)

@app.after_request
def add_cors_headers(response: Response):
    # now allows everything ("public api")
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type')
    response.headers.add('Access-Control-Allow-Headers', 'Cache-Control')
    response.headers.add('Access-Control-Allow-Headers', 'X-Requested-With')
    # response.headers.add('Access-Control-Allow-Headers', 'Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE')
    return response

if __name__ == "__main__":
    SECRET_KEY = os.urandom(32)
    app.config['SECRET_KEY'] = SECRET_KEY

    if(args.debug):
        app.run(debug=args.debug, port=args.port)
    else:
        from waitress import serve
        serve(app, host="0.0.0.0", port=args.port)