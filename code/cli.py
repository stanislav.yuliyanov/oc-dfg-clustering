import datetime
import os
from os.path import exists
import shutil
import pm4py
import ocel_clustering.main as ocel_clustering
import json
import argparse

##########################################################################################################
# Module for executing ocel_clustering algorithm as a cli version.
# parameters are documentet. execute with --help for retrieving detailed information for these parameters
##########################################################################################################

###############
## CONSTANTS ##
###############
DEFAULT_TARGET_DIR = 'ocels_clustered' # default target directory for resulting ocel-files  
CLUSTER_MODES = ['existence', 'all']
DEFAULT_EMPTY_GRAPH = {
    'svg': os.path.dirname(os.path.abspath(__file__)) + '/defaults/empty_ocdfg.svg',
    'png': os.path.dirname(os.path.abspath(__file__)) + '/defaults/empty_ocdfg.png'
}

####################################################
## DEFINING, VALIDATING AND READING CLI-ARGUMENTS ##
####################################################
CLI=argparse.ArgumentParser()
CLI.add_argument(
    "ocel_file",
    help='The ocel-file to apply the clustering on. File type and suffix has to be either ".jsonocel" or ".xmlocel".',
    type=str
)
CLI.add_argument(
    "object_type",
    help='The type of objects from the ocel file which should be clustered. The object type needs to occur in the given ocel file.',
    type=str
)
CLI.add_argument(
    "mode",
    choices=CLUSTER_MODES,
    help='The mode to use for event assingments (either "all" or "existence").',
    type=str
)
CLI.add_argument(
    "--attr_weights",
    type=str,
    default="{}",
    help='The weights for every attribute of the object instances to use for the internal distance calculation. Needs to be a flat attribute->weight json-string. If you would like to set the weight of the internally computed control-flow distance use the following attribute-name: "' + ocel_clustering.OCEL_COL_NAME_CONTROL_FLOW + '". If no weights are set all weights are set equally.',
    required=False
)
CLI.add_argument(
    "--clustering_mode",
    type=str,
    choices=['kmeans', 'dbscan', 'agglomerative', 'hdbscan'],
    default='kmeans',
    help='The algorithm to use to cluster the data internally. Default value is kmeans.',
    required=False
)
CLI.add_argument(
    "--evaluation_mode",
    type=str,
    choices=['silhouette', 'dbindex', 'derivative'],
    default='silhouette',
    help='Evaluation method for cluster validation.',
    required=False
)

def validate_cluster_count(val: str) -> int:
    if val in ['auto', 'automatic']:
        return -1 # symbol for automatic
    res = int(val)
    if not res >= 2:
        raise argparse.ArgumentTypeError('Parameter needs to be at least 2. Less clusters are not possible. You can also use "auto" or "automatic".')
    return res
  
CLI.add_argument(
    "--max_cluster_count",
    type=validate_cluster_count,
    default='auto',
    help='The max count of clusters to create. The software uses the optimum between 2 and the given max_cluster_count. Default is the number of distinct items of the given type (attetion!). Default value is auto.',
    required=False
)
CLI.add_argument(
    "--cluster_count",
    type=validate_cluster_count,
    default='auto',
    help='The count of clusters to create. If the value is "auto" or "automatic" the software uses the optimum between 2 and the given max_cluster_count. Default value is auto.'
)
CLI.add_argument(
    "--ocel_file_type",
    type=str,
    choices=['json', 'xml'],
    default='json',
    help='Defines the type of output file. Default value is json.',
    required=False
)
CLI.add_argument(
    "--graph_file_type",
    type=str,
    choices=['svg', 'png', 'none'],
    default='none', 
    help='Defines the type of the exported graph file type. If not given or set to none this file is not generated. Default value is none',
    required=False
)
CLI.add_argument(
    "--graph_activity_threshold",
    type=int,
    default=0,
    help='Defines the activity threshold for a generated graph (only if --graph_file_type is not "none"). Default value is 0.',
    required=False
)
CLI.add_argument(
    "--graph_edge_threshold",
    type=int,
    default=0,
    help='Defines the edge threshold for a generated graph (only if --graph_file_type is not "none"). Default value is 0.',
    required=False
)
def validate_directory(dir: str) -> str:
    res = dir
    while(len(res) > 0 and res[0] == '/'):
        res = res[1:]
    while(len(res) > 0 and res[-1] == '/'):
        res = res[:len(res)-1]
    return str(os.getcwd() + '/' + res)

CLI.add_argument(
    "--target_dir",
    type=validate_directory,
    default=DEFAULT_TARGET_DIR,
    required=False,
    help='Defines the target directory relative to the current directory or a full path. Default value is: ' + DEFAULT_TARGET_DIR
)
args = CLI.parse_args()

########################################
## DATA-BASED-VALIDATING OF ARGUMENTS ##
########################################
# Check file existence
assert exists(args.ocel_file), 'Given file does not exists.'

# Reading file to OCEL-structure and possible object types
ocel = pm4py.read_ocel(args.ocel_file)
ocel_object_types = list(ocel.objects['ocel:type'].unique())

# Validating if given args.object_type is possible
assert args.object_type in ocel_object_types, 'Given type "' + args.object_type + '" is not present in the data please use one of: "' + '", "'.join(ocel_object_types) + '".'

# reading args.attr_weights as json
try:
    print(args.attr_weights)
    args.attr_weights = json.loads(args.attr_weights)
except json.JSONDecodeError as error_msg:
    raise error_msg # further improvement needed

# checking if clustering mode is possible
assert args.clustering_mode in ocel_clustering.OCEL_CLUSTER_ALGORITHMS, 'The given clustering mode "' + args.clustering_mode + '" is not availabel. Use on of: ' + ', '.join(ocel_clustering.OCEL_CLUSTER_ALGORITHMS) + '.'

print('-------------------------------------------------------')
print('                       SETTINGS                        ')
print('ocel_file:                ' + str(args.ocel_file))
print('mode:                     ' + str(args.mode))
print('object_type:              ' + str(args.object_type))
print('attr_weights:')
print(args.attr_weights)
print('clustering_mode:          ' + str(args.clustering_mode))
print('evaluation_mode:          ' + str(args.evaluation_mode))
print('cluster_count:            ' + str(args.cluster_count))
print('max_cluster_count:        ' + str(args.max_cluster_count))
print('target_dir:               ' + str(args.target_dir))
print('ocel_file_type:           ' + str(args.ocel_file_type))
print('graph_file_type:          ' + str(args.graph_file_type))
print('graph_activity_threshold: ' + str(args.graph_activity_threshold))
print('graph_edge_threshold:     ' + str(args.graph_edge_threshold))
print('-------------------------------------------------------')

#################################
## PREPARING ATTRIBUTE WEIGHTS ##
#################################
# getting default full attribute definition
attr_def = ocel_clustering.ocel_get_attr_def(ocel)
def set_weights_function(attr_def: dict) -> dict:
    res = attr_def
    if res['name'] in args.attr_weights.keys():
        res['weight'] = args.attr_weights[res['name']]
    return res
attr_def = list(map(set_weights_function, attr_def))

control_flow_weight = args.attr_weights.get(ocel_clustering.OCEL_COL_NAME_CONTROL_FLOW, 1.0)

##############################
## EXECUTING CORE ALGORITHM ##
##############################
print('calculating...')
start_ts = datetime.datetime.now()
res, distance_matrix, cluster_evaluation = ocel_clustering.ocel_cluster_by_objects(
    ocel=ocel, 
    object_type             = args.object_type, 
    event_assignment_mode   = args.mode,
    attr_def                = attr_def, 
    clustering_algorithm    = args.clustering_mode,
    evaluate                = args.evaluation_mode,
    max_cluster_count       = args.max_cluster_count,
    cluster_count           = args.cluster_count,
    control_flow_active     = control_flow_weight != 0.0,
    control_flow_weight     = control_flow_weight
)
print('duration: ' + str(datetime.datetime.now() - start_ts))

##########################
## STORING OUTPUT FILES ##
##########################
print('-------------------------------------------------------')
print('             STORING CLUSTERED-OCEL-FILES              ')
if not os.path.exists(args.target_dir):
    os.makedirs(args.target_dir)
appendix_len = len(str(len(res)))
for ii in range(0, len(res)):
    filename = args.target_dir + '/cluster_' + str(ii+1).rjust(appendix_len, '0') + '.' + args.ocel_file_type + 'ocel'
    pm4py.write_ocel(res[ii], filename)
    print(str(ii+1).rjust(appendix_len, '0') + '/' + str(len(res)) + ' "' + filename + '" stored.')

####################################################
## OPTIONAL DISCOVERING OCDFGS AND STORING IMAGES ##
####################################################
if args.graph_file_type != 'none':
    print('-------------------------------------------------------')
    print('             GENERATING AND STORING GRAPHS             ')
    for ii in range(0, len(res)):
        filename = args.target_dir + '/cluster_' + str(ii+1).rjust(appendix_len, '0') + '_ocdfg.' + args.graph_file_type
        if res[ii].events.shape[0] == 0:
            # no events -> empty dfg and save_vis_ocdfg fails on that.
            shutil.copyfile(DEFAULT_EMPTY_GRAPH[args.graph_file_type],filename)
        else:
            ocdfg = pm4py.discover_ocdfg(res[ii])
            pm4py.save_vis_ocdfg(ocdfg, filename, act_threshold=args.graph_activity_threshold, edge_threshold=args.graph_edge_threshold)
            print(str(ii+1).rjust(appendix_len, '0') + '/' + str(len(res)) + ' "' + filename + '" stored.')

###############################
## CLUSTER EVALUATION OUTPUT ##
###############################

filename = args.target_dir + '/clustering_' + args.clustering_mode + '_' + args.evaluation_mode + '_plot.svg'
cluster_evaluation.plot(savefig={'fname': filename, 'format': 'svg'})

filename = args.target_dir + '/clustering_' + args.clustering_mode + '_' + args.evaluation_mode + '_scatter.svg'
cluster_evaluation.scatter(distance_matrix, savefig={'fname': filename, 'format': 'svg'})

quit()
