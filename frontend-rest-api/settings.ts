const ROOT_URL =
  process.env.NODE_ENV == "production"
    ? "http://127.0.0.1:8000"
    : "http://127.0.0.1:5000";
export default ROOT_URL;
