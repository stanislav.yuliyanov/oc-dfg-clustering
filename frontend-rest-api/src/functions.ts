import ROOT_URL from "@/../settings";
import { stringifyQuery } from "vue-router";

export function getFileExtension(filename: string) {
  const parts = filename.split(".");
  if (parts.length > 1) {
    return parts.pop();
  } else throw Error("File has no extension.");
}

/* uploads a single file to the given url via 'default' technique used by html forms */
export async function upload_single_file(
  file: File,
  url: string,
  file_attribute_name = "file",
  method = "POST"
) {
  const formData = new FormData();
  formData.append(file_attribute_name, file, file.name);
  return fetch(url, {
    method: method,
    body: formData,
    mode: "cors",
    // headers: { "content-type": undefined }, // DO NOT ADD THIS HEADER! ITS AUTOMATICALLY SET!
  });
}
interface GetParameter {
  [key: string]: string | GetParameter;
}

function flatten_get_params(p: GetParameter): Record<string, string> {
  const res: Record<string, string> = {};
  for (const attr in p) {
    if (typeof p[attr] === "string" || p[attr] instanceof String) {
      res[attr] = String(p[attr]);
    } else if (p[attr] as GetParameter) {
      const tmp = flatten_get_params(p[attr] as GetParameter);
      for (const sub_attr in tmp) {
        res[attr + "[" + sub_attr + "]"] = tmp[sub_attr];
      }
    }
  }
  return res;
}

export function params_to_string(p: GetParameter): string {
  const flattened = flatten_get_params(p);
  const res = Array<string>();
  for (const attr in flattened) {
    res.push(
      encodeURIComponent(attr) + "=" + encodeURIComponent(flattened[attr])
    );
  }

  return res.join("&");
}

export async function api_call(
  route: string,
  method: "GET" | "POST" | "DELETE",
  body: JSON | null = null,
  params: GetParameter | null = null,
): Promise<Response> {
  let url = ROOT_URL + route;
  if (params != null) url = url + "?" + params_to_string(params);

  return fetch(url, {
    method: method,
    mode: "cors",
    body: body != null ? JSON.stringify(body) : undefined,
    headers: new Headers({ "content-type": "application/json" }),
  });
}

export function number_or_string_to_int(
  val: number | string,
  default_val = 0,
  radix = 10 // Only used for Strings.
): number {
  radix = Math.floor(radix);
  if (radix < 2 || radix > 32)
    throw Error(
      "radix needs to be greater or equal to two and smaller or equal to 32"
    );
  if (typeof val === "string") {
    if (val == "") return default_val;
    else return parseInt(val, radix);
  } else return Math.round(val);
}
