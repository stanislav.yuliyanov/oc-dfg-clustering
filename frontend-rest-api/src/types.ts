/* API TYPES */

export type API_File = {
  id: string;
  type: string;
};
export type API_FileList = Array<API_File>;

export type API_Object_Type = {
  id: string;
  type: string;
};
export type API_Object_Types = Array<API_Object_Type>;

export type API_Attribute_Type = {
  id: string;
  type: string;
};
export type API_Attribute_Types = Array<API_Attribute_Type>;

export type API_Cluster_Type = {
  id: string;
  type: string;
  attributes: {
    filetype: string;
  };
};
export type API_Cluster_Types = Array<API_Cluster_Type>;
