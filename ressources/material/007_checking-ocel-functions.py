#!/usr/bin/env python
# coding: utf-8

# In[1]:


# pm4py needed throughout the majority of the time
# pandas needed for data frame manipulation
import pm4py
import pandas as pd


# In[2]:


# Self explanatory
path = "./running-example.jsonocel"
ocel = pm4py.read_ocel(path)


# In[3]:


# Self explanatory
attribute_names = pm4py.ocel_get_attribute_names(ocel)
object_types = pm4py.ocel_get_object_types(ocel)
object_type_activities = pm4py.ocel_object_type_activities(ocel)
ocel_objects_ot_count = pm4py.ocel_objects_ot_count(ocel)


# In[4]:


# Self explanatory
print(ocel)


# In[7]:


# Function to set the preferred object type
# Later on: can implement exception handling to output error
# in the terminal. The idea is to execute the entire program 
# just with a few given arguments and not require the user
# to interact with it as much. For now it is ok to leave it like
# that.

def set_obj_type():
    pref_type = input("Input for preferred object type: ")
    if(pref_type in pm4py.ocel_get_object_types(ocel)):
        print(f"Preferred object type: {pref_type}")
        return pref_type
    else:
        print("This object type does not exist in the current OCEL.")
        return set_obj_type()


# In[8]:


# Store object type preference to a variable
pref_type = set_obj_type()


# In[9]:


# Self explanatory
print(type(attribute_names))
print(attribute_names)


# In[10]:


# Self explanatory
print(type(object_types))
print(object_types)


# In[11]:


# Self explanatory
print(type(object_type_activities))
print(object_type_activities)


# In[12]:


# Self explanatory
print(type(ocel_objects_ot_count))
print(ocel_objects_ot_count)


# In[13]:


# Prints type of the variable where the entire ocel has been stored
print(type(ocel))


# In[14]:


# Stores the events data frame from the OCEL into a separate variable
# and displays it
ocel_events_df = pd.DataFrame(ocel.events)
display(ocel_events_df)


# In[15]:


# Stores the objects data frame from the OCEL into a separate variable
# and displays it
ocel_objects_df = pd.DataFrame(ocel.objects)
display(ocel_objects_df)


# In[16]:


# Stores the relations data frame from the OCEL into a separate variable
# and displays it
ocel_relations_df = pd.DataFrame(ocel.relations)
display(ocel_relations_df)


# In[17]:


# Filters list to store all objects which match the preferred type
relations_filtered = ocel_relations_df.loc[ocel_relations_df['ocel:type'] == pref_type]


# In[18]:


# Displays the result
display(relations_filtered)


# In[19]:


# Gets all unique object id

unique_oid = relations_filtered.drop_duplicates(subset = ["ocel:oid"])["ocel:oid"]
display(unique_oid)


# In[ ]:


list_cf = []
i = 1
single_obj_filtered = ocel_relations_df.drop(labels=["ocel:eid", "ocel:type"], axis=1, inplace=False)
single_obj_filtered = single_obj_filtered.sort_values(by = ["ocel:oid", "ocel:timestamp"])
single_obj_filtered = single_obj_filtered.loc[ocel_relations_df['ocel:type'] == pref_type]

for elem in unique_oid:
    
    tmp_obj = single_obj_filtered.loc[ocel_relations_df['ocel:oid'] == elem]
    #display(single_obj_filtered)
    tmp_obj = tmp_obj.drop_duplicates(subset = ["ocel:activity"])["ocel:activity"]
    #display(tmp_activity)
    str_activity = ""
    #print(str_activity)
    for elem in tmp_obj:
        str_activity += elem + " -> "
    #print(str_activity)
    if str_activity not in list_cf:
        list_cf.append(str_activity)
    list_cf
    i += 1
    if (i == 1000):
        break


# In[ ]:


list_cf


# In[ ]:


tmp_activity = relations_filtered.drop_duplicates(subset = ["ocel:activity"])["ocel:activity"]
display(tmp_activity)


# In[ ]:




