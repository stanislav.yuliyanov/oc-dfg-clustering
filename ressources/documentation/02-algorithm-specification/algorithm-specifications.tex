\documentclass[a4paper, 12pt]{article}
\usepackage[]{tabularx}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}

\title{Algorithm description}
\author{Simon Glomb, Stanislav Yuliyanov, Lennart Holzenkamp}
	
\begin{document}

\maketitle

\section*{Intro}
    Algorithm was specified by Anahita Farhang

\section*{Algorithm-description}

    \subsection{Input}
        \subsubsection{OCEL-data}
            Data the program works with.

            \begin{table}[!htbp]
                \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                    name        & ocel\_file\\
                    type        & file path (string)\\
                    options     & any valid ocel file, hence .json or .xml correctly formatted ocel file.\\
                    validation  & Assignment of this attribute, Existence of the given file and if its format is valid.
                \end{tabularx}
            \end{table}

        \subsubsection{Selection of the event assignment mode}
            Attribute defines which approach for assigning events to clusters is used during the execution.

            \begin{table}[!htbp]
                \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                    name        & mode\\
                    type        & string\\
                    options     & all $|$ existence\\
                    validation  & Assignment of this attribute, Check if its either \textit{all} or \textit{existence}.
                \end{tabularx}
            \end{table}

        \subsubsection{Selection of the object-type to use for clustering}
            Objects of the defined type are clustered and events are assigned to these clusters. Other object-types
            are not considered for the algorithm.

            \begin{table}[!htbp]
                \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                    name        & object\_type\\
                    type        & string\\
                    options     & all | existence \\
                    validation  & Assignment of this attribute, Check if its either \textit{all} or \textit{existence}.
                \end{tabularx}
            \end{table}
            
        \subsubsection{Definition of attribute weights for clustering (optional)}
            While determining the clusters of object-instances, distance of attributes are measured.
            The user should be able to define the weights of these distances.
            If no weight-definition is giving the distances are normalised and then averaged.
            The weights can be defined per attribute. If single attributes are not present in
            the given weight-definition they should be set to one.

            \begin{table}[!htbp]
                \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                    name        & attr\_weights\\
                    type        & Some kind of Map: \{<attr\_1\_name>: <attr\_1\_weight>, ..., <attr\_n\_name>: <attr\_weight>\}.\\
                    options     & floating point numbers per attribute.\\
                    validation  & If a attribute is not present in the dataset its ignored. Formatting should be checked.\\
                    default     & If this argument is not set by the user, every normalized attribute distance is used with factor one.
                \end{tabularx}
            \end{table}

            \textbf{Default} value is same weight for every attribute (factor one for every attribute)

        \subsubsection{Selection of clustering techniques (optional)}
            User can select between k-means or other clustering techniques.

            \begin{table}[!htbp]
                \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                    name        & clustering\_mode\\
                    type        & One of the given options\\
                    options     & k-means | ...\\
                    validation  & Option has the be one of the defined.\\
                    default     & k-means
                \end{tabularx}
            \end{table}

        \subsubsection{OCEL-Output file type (optional)}
            \begin{table}[!htbp]
                \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                    name        & ocel\_file\_type\\
                    type        & One of the given options\\
                    options     & json | xml\\
                    validation  & Option has the be one of the defined.\\
                    default     & xml
                \end{tabularx}
            \end{table}

        \subsubsection{Graph-Output file type (optional)}
        \begin{table}[!htbp]
            \begin{tabularx}{\textwidth}{>{\bfseries}l|X}
                name        & graph\_file\_type\\
                type        & One of the given options\\
                options     & svg | png\\
                validation  & Option has the be one of the defined.\\
                default     & svg
            \end{tabularx}
        \end{table}
    
        \subsubsection{Definition of minimum number of edges and nodes (optional)}
            Necessary for drawing the graphs. Attribute names: \textit{graph\_min\_nodes} and \textit{graph\_min\_edges}.
            \textbf{Default} value needs further Specifications

    \subsection{Algorithm}
        \subsubsection{Reading and validating inputs}
            \begin{itemize}
                \item Read in all inputs.
                \item Build OCEL-Object with pm4py.
                \item Validate inputs (\textit{OCEL-Object-tree nees to be given for soem validation}).
            \end{itemize}

        \subsubsection{Retrieving OCEL-information}
            \begin{itemize}
                \item Discover OCEL-tree to gain further data.
                \item control flow of events per object instance ordered by theire timestamp as a ordered list consisting of the type-name of the events.
            \end{itemize}

        \subsubsection{Creating object table}
            \begin{itemize}
                \item Filter object-instances from ocel-object that are of the user defined type (parameter: \textit{object\_type}).
                \item Ensure that this list is distinct.
                \item Ensure that a unique identifier, all attributes and the previously retrieved event-control-flow is contained in the table.
            \end{itemize}

        \subsubsection{Calculate distances}
            \begin{itemize}
                \item Iterate through every object-object combination and calculate for every attribute including the control-flow distances.
                    \begin{itemize}
                        \item string-attributes: return 1 for equal, 0 else.
                        \item number-attributes: absolute difference.
                        \item control-flow: Levenshtein distance considering \\the event-type-name as letter.
                    \end{itemize}
                \item Normalize the measurements to values between 0-1 (divide by the maximum per attribute).
                \item If any weights for attributes are set (parameter: \textit{attr\_weights}) apply these weights.
                \item Average all the attribute-distances per object-object-combination.
                \item Build a matrix which holds the object-object distances and create a mapping from object-id to 
                      matrix-index and also object-id to matrix-index.
            \end{itemize}

        \subsubsection{Object-Clustering}
            \begin{itemize}
                \item Apply the clustering technique possibly set by the parameter \textit{clustering\_mode}
                \item Use a datastructure which enables to assign events to these clusters.
            \end{itemize}

        \subsubsection{Event assignment}
            \begin{itemize}
                \item Iterate over all events.
                \item Assign an event to a object-cluster based on the relation of this event to the objects in the cluster.
                    Use one of the following approaches, based on the users decision in the parameter \textit{mode}
                    \begin{itemize}
                        \item \textbf{all}: If - and only if - all the objects that are related to a event are present in the cluster, assign the event.
                        \item \textbf{existence}: If one of the objects that are related to a event is present in the cluster, assign the event. 
                    \end{itemize}
                \item 
            \end{itemize}
        
        \subsubsection{Exporting}
            \begin{itemize}
                \item For every clusters create new ocel-objects.
                \item Based on the users decision (parameter \textit{ocel\_file\_type}) create .xml or .json file containing the ocel-data.
                \item Based on the users decision (parameter \textit{graph\_file\_type}) create .svg or .png file containing a graph representation of the ocel-data. This graph should be created using the parameter \textit{graph\_min\_edges} and \textit{graph\_min\_nodes}.
                \item Store the created files.
            \end{itemize}

% add other sections and subsections to suit
\end{document}